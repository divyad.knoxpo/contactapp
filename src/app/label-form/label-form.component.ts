import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators, FormGroup } from '@angular/forms';
import { ILabel } from '../label';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-label-form',
  templateUrl: './label-form.component.html',
  styleUrls: ['./label-form.component.scss']
})
export class LabelFormComponent implements OnInit {
  labelForm: FormGroup;
  labels: ILabel[] = [];
  isEditForm: boolean = false;

  constructor(private fb: FormBuilder, private contactService: ContactService) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.labelForm = this.fb.group({
      labelName: ['']
    });
  }
  saveLabel() {
    this.contactService.addLabel(this.labelForm.value);
    console.log(this.labels);
  }
  cancelLabel() {
    this.labelForm.reset();
  }

}
