import { Injectable } from '@angular/core';
import { ILabel } from './label';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  labels: ILabel[] = [];
  constructor() { }

  addLabel(labels: ILabel) {
    const newId = this.labels.length + 1;
    console.log(newId);
    console.log(this.labels);
    this.labels.push({
      id: newId,
      labelName: labels.labelName
    });
    console.log(labels);
  }
  getLabel() {
    return this.labels;
  }
  deleteLabel(id: number) {
    this.labels = this.labels.filter(label => label.id !== id);
    this.labels.splice(1, id);
    console.log(this.labels);

  }
}
