import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LabelFormComponent } from './label-form/label-form.component';

const routes: Routes = [
  { path: '', component: LabelFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
