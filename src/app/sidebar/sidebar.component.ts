import { Component, OnInit, Input } from '@angular/core';
import { ILabel } from '../label';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  labels: ILabel[];

  constructor(private contactService: ContactService) {
    this.labels = this.contactService.getLabel();
  }


  ngOnInit() {
  }
  deleteLabel(id: number) {
    this.contactService.deleteLabel(id);
  }
}
